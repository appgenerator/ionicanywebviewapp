# ionicanywebview
This shows the basic setup and CI/CD process of an ionic webapp and android application.
It will directly load a webview which can be configured under src/environments->SERVER_URL. Default it will load duckduckgo.com.

# Requirements

* [Node.js](https://nodejs.org/en/download/) installed on your system.
* Cordova and The Ionic CLI installed by the following command:


```
    npm install -g cordova
    npm install -g ionic
```

    
* Java and Android SDK (If you want to build for Android)
    * Set ANDROID_HOME Variable on your System
* A Mac system if you want to target iOS.
* A Windows system and Visual Studio installed if you want to target Windows.


# Initial Steps (required only once)
## Install correct Android SDK with Android Studio
    1. Open your Android SDK Manager. In Android Studio you can do this under Tools->Android->SDK Manager
    2. In the popup select the checkbox "Android API 28" or another platform that you would like to test on. 
    3. Click Apply and accept all license agreements in the following install Dialog

##  Install correct Android SDK with Command Line (Alternative)
    Based on https://developer.android.com/studio/command-line/sdkmanager
    1. Open CMD/Bash
    2. cd android_sdk/tools/bin/
    3. sdkmanager "platform-tools" "platforms;android-28"
    
## Install cordova android reqirements

    npm install
    ionic cordova platform add android@8.0.0
    ionic cordova prepare android
    ionic cordova run android

# Development Server
Ionic offers a Development server which can be started in the following way:

    ionic serve
    
# Building
By executing this command you will recieve an apk in the subfolder of platforms/android/app/build/...
This will also be done automatically please see the CI/CD folder in gitlab to see the results.

    ionic cordova build android 
    
Alternatively you can run android directly which will build and then run the application either in emulator if no detectable device is attached or on the device directly by 

    ionic cordova run android
    
# Testing
This project contains End-to-End tests(e2e) using protractor and Unit tests which can be run 
by the following commands:

    ng test
    ng pree2e
    ng e2e

This commands will also be automatically executed on a commit. please see the CI/CD folder for the results.