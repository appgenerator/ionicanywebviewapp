import { Component,OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { SERVER_URL } from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{
  constructor(private iab: InAppBrowser) {}

  ngOnInit(): void {
    const browser = this.iab.create(SERVER_URL, '_self', {location: 'no'}); /*3*/
  }

}
